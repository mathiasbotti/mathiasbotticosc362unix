from WorkerGenerator import *
#===========================================
#GENERATE THE FILES
#===========================================

#WorkerGenerator.workerCard()

#===========================================
# READ IN FILES
#===========================================
files = os.listdir('.')
print("\nContents of current directory: ")
for i in range(len(files)):
	print(str(files[i]))
print("\nContents of directory to work with: ")
workerFiles = []
for j in range(len(files)):
	if str(files[j]).endswith('.yeet'):
		workerFiles.append(files[j])
for k in range(len(workerFiles)):
	print(workerFiles[k])

#==========================================
#MOVE FILES INTO SINGULAR MASTER FILE
#==========================================
masterFile = open('MasterFile.yeeet', 'w')
for i in range(len(workerFiles)):
	workerFileTemp = open(workerFiles[i], 'r')
	masterFile.write(workerFileTemp.read())
	workerFileTemp.close()

masterFile.close()
masterFile = open('MasterFile.yeeet', 'r')
for line in masterFile:
	print line
masterFile.close()
