#!/bin/bash

#find . -name "*.aux" -type f

for f in $(find . -name "*.tex" -type f)
do
	pdflatex $f
#debug line vvv
#	echo $f
	
done


#instead of using different loops, i could use a regex. but i can't be bothered. sorry.
for g in $(find . -name "*.aux" -type f)
do
	echo "deleting " $g
	rm $g
done

for h in $(find . -name "*.log" -type f)
do
	echo "deleting " $h
	rm $h
done

for i in $(find . -name "*.bib" -type f)
do
	echo "deleting " $i
	rm $i
done


