from workerDocumentText import *
import os
import sys

"""
if len(sys.argv) != 2:
	print("To Run: python workersMain.py workerFiles.txt")
else:
	print "Using worker data from the file ", str(sys.argv[1])
	dataFile = str(sys.argv[1])
	
workerData = open(dataFile,'r')

print "Reading from a file: " + dataFile
"""
#=======================================================================
# READ IN THE FILES
#=======================================================================
files = os.listdir('.')
print 'Contents of current directory: '
for i in range(len(files)):
	print str(files[i])
print 'Usable contents of current directory: '
workerFiles = []
for i in range(len(files)):
	if str(files[i].endswith('.yeet')):
		workerFiles.append(files[i])

for i in range(len(workerFiles)):
	print(workerFiles[i])

#========================================================================
# MOVE FILES INTO SINGULAR MASTER FILE (TO RULE THEM ALL)
#========================================================================
masterFile = open('MasterFile.yeeet', 'w')
for i in range(len(workerFiles)):
	workerFileTemp = open(workerFiles[i], 'r')
	masterFile.write(workerFileTemp.read())
	workerFileTemp.close()

masterFile.close()
masterFile = open('MasterFile.yeeet', 'r')
for line in masterFile:
	print line
masterFile.close()


#========================================================================
temp = 0
workList = []
firstLine = 0
personTotal = 0
#========================================================================

def Total(hours,mins):
	h = int(hours.replace('hr',''))
	m = int(mins.replace('mins',''))
	s = h * 60 + m
	return s

#=========================================================================

masterFile = open('MasterFile.yeeet', 'r')
for line in masterFile:
	if((firstLine == 0) or (firstLine % 8 == 0)):
		fname = str(line)
		print "Fname: ", fname
	else:
		print line
		day, hours, mins = map(str, line.split(' '))
		temp = Total(hours,mins)
		personTotal = temp + personTotal
	firstLine+=1
	if(firstLine > 1 and firstLine % 8 == 0):
		recentWorker = WorkersSetup(fname,personTotal)
		workList.append(recentWorker)
		personTotal = 0
masterFile.close()
#===========================================================================

for i in range(len(workList)):
	workerText = workList[i]
	workerText.WorkersTotal()
