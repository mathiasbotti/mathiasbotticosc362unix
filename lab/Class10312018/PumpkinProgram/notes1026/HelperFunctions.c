/* This is a bunch of stuff to use in pixel mapping */

#include <math.h>  // Allows use of pow(x,y) ---> x^y

int InCircle(int totalrows, int totalcols, int radius, int pixRow, int pixCol){
	int InOrOut = 0;  //Integer flag for if the current pixel is in the circle or not.
			  //1=yes, 0=no

	int Centerx = totalcols/2;
	int Centery = totalrows/2;
	int dist = 0;			//Distance from center to pixel

	dist = pow((pow(Centerx - pixCol,2)) + (pow(Centery - pixRow,2)),0.5);

	if (dist < radius){
		InOrOut = 1;
	}

	return InOrOut;
}

int InSquares(int totalrows, int totalcols, int radius, int pixRow, int pixCol){
	int InOrOut = 0; //Integer flag.  
			 //0=no square here, my guy
			 //1=stem here
			 //2=eyes, nose, or mouth here
			 //would this be considered a troolean? a boolean+1?
			 //probably just a flag.

	int topOfCircle = 0;
	int Centerx = totalcols/2;
	int Centery = totalrows/2;
	//=======================================
	//stem stuff
	//=======================================

	int stemSize = radius/8;  //size of the stem (and eyes)
	int stemCenter = Centery + (radius/2);  //Position of the center of the stem
	//checking to see if in stem
	//checking verticle position
	if ( (pixRow >= (stemCenter - stemSize/2)) && (pixRow <= (stemCenter + stemSize/2)) ){
		//if verticle position is within bounds, check horizontal position
		if( (pixCol >= (Centerx - stemSize/2)) && (pixCol <= (Centerx + stemSize/2)) ){
			InOrOut = 1;
		}
	}
	//=======================================
	//eye stuff
	//=======================================

	int eyeCenterY = Centery + radius/4;  //verticle position of eyes, 3/4 of way up circle
	int LEyeCenterX = Centerx - radius/4; //horizontal position of left eye
	int REyeCenterX = Centerx + radius/4; //horizontal position of right eye

	//checking if within verticle position of eyes
	if ( (pixRow >= (eyeCenterY - stemSize/2)) && (pixRow <= (eyeCenterY + stemSize/2)) ){
		//if verticle position w/in bounds, check horizontal position
		//left eye
		if ( (pixCol >= (LEyeCenterX - stemSize/2)) && (pixCol <= (LEyeCenterX + stemSize/2)) ){
			InOrOut = 2;
		}

		//right eye
		if ( (pixCol >= (REyeCenterX - stemSize/2)) && (pixCol <= (REyeCenterX + stemSize/2)) ){
			InOrOut = 2;
		}
	}

	//=====================================
	//nose stuff
	//=====================================
	if ( (pixRow >= (Centery - stemSize/2)) && (pixRow <= (Centery + stemSize/2)) ){
		if ( (pixCol >= (Centerx - stemSize/2)) && (pixCol <= (Centerx + stemSize/2)) ){
			InOrOut = 2;
		}
	}



	//======================================
	//mouth stuff
	//======================================

	int mouthCenter = Centery + radius/4; //verticle position of the mouth, 3/4 of way down the circle
	int mouthWidth = radius/3; //width of mouth (height is same as stem and eyes)
	//checking if within verticle position of mouth
	if ( (pixRow >= (mouthCenter - stemSize/2)) && (pixRow <= (mouthCenter + stemSize/2)) ){
		//if w/in verticle bounds, check horizontal bounds
		if ( (pixCol >= (Centerx - mouthWidth)) && (pixCol <= (Centerx + mouthWidth)) ){
			InOrOut = 2;
		}
	}



	return InOrOut;
}
