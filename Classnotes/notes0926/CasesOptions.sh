#!/bin/bash

while [ -n "$1" ]; do 
     case "$1" in

     -a) echo "-a option was given to me" ;;

     -b) echo "-b option was shown to this script" ;;

     -mathias) echo "this is the worst option" ;;

     --) 
        shift # Separate out the parameters to my shell script.

        break
        ;;  # Exits this loop and looks now for parameters

     esac

     shift


done

total=1

for param in $@; do

     echo "#$total: $param"
     total=$((total + 1))

done
