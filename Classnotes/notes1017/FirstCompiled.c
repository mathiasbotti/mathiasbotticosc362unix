#include <stdio.h>
//^^standard i/o^^
#include <stdlib.h>
//^^standard library? idk lol ^^

/*
*  This is a c program that does something cool.
*  Many line comment.
*/

int main(int argc,	//This is the number of things passed into this function
         char *argv[]	//This is the array of things passed
        ){
	printf("============\n");  // This is a single line comment
	printf("Hello, world\n");
	printf("============\n\n");

	return 0;
}
